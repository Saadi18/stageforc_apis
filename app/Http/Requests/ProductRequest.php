<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|min:8|max:191',
            'product_description' => 'required|min:8|max:500',
            'purchase_date' => 'required',
            'purchase_price' => 'required',
            'quantity' => 'required',
        ];
    }

    public function updateRules()
    {
        return [
            'product_name' => 'required|min:8|max:191',
            'product_description' => 'required|min:8|max:500',
            'purchase_date' => 'required',
            'purchase_price' => 'required',
        ];
    }
}
