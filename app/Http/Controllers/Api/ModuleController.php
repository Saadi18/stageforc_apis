<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Models\Module;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $modules = Module::with('permissions')->get();
            return HelperModule::jsonApiResponse(200,'Module listing',$modules);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->name == '')
                return HelperModule::jsonApiResponse(422,'Module name is required',null);
            $data['name'] = $request->name;
            $role = Module::create($data);
            return response()->json([
                'status'      =>  200,
                'message'     => 'Module created successfully',
                'data'        => new RoleResource($role),
            ]);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $role = Module::with('permissions')->find($id);
            if($role)
                return HelperModule::jsonApiResponse(200,'Module detail',$role);

            return HelperModule::jsonApiResponse(200,'Module not exists');
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->name == '')
                return HelperModule::jsonApiResponse(422,'Module name is required',null);
            $module = Module::find($id);
            $module->name = $request->name;
            $module->update();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Module updated successfully',
                'data'        => new RoleResource($module),
            ]);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $module = Module::find($id);
            if($module){
                $module->delete();
                return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
            }
            return HelperModule::jsonApiResponse(404,'Module Not exists',null);

        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
