<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::where('id', '!=', 1)->with('roles')->paginate(config('pagination'));
            $pagination = [];
            $pagination['perpage'] = $users->perPage();
            $pagination['nextPageUrl'] = $users->nextPageUrl();
            $nextPageNumber = explode('=', $users->nextPageUrl());
            $pagination['nextpage'] = isset( $nextPageNumber[1] ) ? intval($nextPageNumber[1]) : $users->nextPageUrl() ;
            $pagination['previouspage'] = $users->previousPageUrl();
            $pagination['currentPage'] =$users->currentPage();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Users listing',
                'Users'        => UserResource::collection($users),
                'pagination'        => $pagination
            ]);
        } catch (\Exception $ex) {
            HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name'    =>  'required',
                'last_name'     =>  'required',
                'phone'    =>  'required',
//                'city'    =>  'required',
//                'state'    =>  'required',
                'email'    =>  'required|unique:users|email',
                'password'     =>  'required',
//                'street'    =>  'required',
//                'warehouse'     =>  'required',
                'role'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }
            }
            $data = $request->all();
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/user_images');
                $image->move($destinationPath, $name);
                $data['image'] = $name;
            }
            $data['password'] = bcrypt($request->password);
            $user = User::create($data);
            if($user){
                $role = Role::find($request->role);
                $user->assignRole($role->id);
                $user->roles;
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'User Stored Successfully',
                    'User'        => new UserResource($user),
                ]);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $category = User::find($id);
            if($category)
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'User found',
                    'User'        => new UserResource($category),
                ]);

            return HelperModule::jsonApiResponse(404,'Location Not Exist',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
                'name'    =>  'required',
                'last_name'     =>  'required',
                'phone'    =>  'required',
//                'city'    =>  'required',
//                'state'    =>  'required',
                'email'    =>  'required',
//                'street'    =>  'required',
//                'warehouse'     =>  'required',
                'role'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }
            }
           $user = User::find($request->id);
            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $user->phone = $request->phone;
//            $user->state = $request->state;
//            $user->city = $request->city;
            $user->email = $request->email;
//            $user->street = $request->street;
            $user->warehouse = $request->warehouse;
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/user_images');
                $image->move($destinationPath, $name);
                $user->image = $name;
            }
            $user->update();
            DB::table('model_has_roles')->where('model_id',$request->id)->delete();
            $user->assignRole($request->role);
            return response()->json([
                'status'      =>  200,
                'message'     => 'User updated Successfully',
                'User'        => new UserResource($user),
            ]);
        } catch (\Exception $ex) {
            HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::find($id);
            if($user){
                $user->delete();
                return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
            }
            return HelperModule::jsonApiResponse(200,'No record found',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
