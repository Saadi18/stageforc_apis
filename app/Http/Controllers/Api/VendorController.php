<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\VendorResource;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $vendors = Vendor::paginate(config('pagination'));
            $pagination = [];
            $pagination['perpage'] = $vendors->perPage();
            $pagination['nextPageUrl'] = $vendors->nextPageUrl();
            $nextPageNumber = explode('=', $vendors->nextPageUrl());
            $pagination['nextpage'] = isset( $nextPageNumber[1] ) ? intval($nextPageNumber[1]) : $vendors->nextPageUrl() ;
            $pagination['previouspage'] = $vendors->previousPageUrl();
            $pagination['currentPage'] = $vendors->currentPage();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Vendor listing',
                'vendors'        => VendorResource::collection($vendors),
                'pagination'        => $pagination
            ]);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required|max:1000',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }
            }
            $data = $request->all();
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/vendor_images');
                $image->move($destinationPath, $name);
                $data['image'] = $name;
            }
            $vendor = Vendor::create($data);
            if($vendor){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Vendor Stored Successfully',
                    'Vendor'        => new VendorResource($vendor),
                ]);
            }
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $product = Vendor::find($id);
            if($product)
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Vendor found',
                    'vendor'        => new VendorResource($product),
                ]);

            return HelperModule::jsonApiResponse(404,'Vendor Not Exist',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $vendor = Vendor::find($request->id);
            $vendor->name = $request->name;
            if(isset($request->description) && $request->description != null){
                $vendor->description = $request->description;
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/vendor_images');
                $image->move($destinationPath, $name);
                $vendor->image = $name;
            }
            $vendor->update();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Vendor updated Successfully',
                'vendor'        => new VendorResource($vendor),
            ]);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $product = Vendor::find($id);
            $product->delete();
            return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function getVendorsList(Request $request){
        $vendors = Vendor::all();
        if($vendors){
            foreach ($vendors as $key => $vendor){
                $result[$key]['id'] = $vendor->id;
                $result[$key]['name'] = $vendor->name;
            }
           return response()->json(['status' => 200, 'message'=>'Vendor list', 'data' => $result]);
        }else{
            HelperModule::jsonApiResponse(404,'No Vendor exists',null);
        }
    }
}
