<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return HelperModule::jsonApiResponse(false, Lang::get('auth.failed'));
        $user = $request->user();
        $permissions = $user->getPermissionsViaRoles();
        $final_per = [];
        foreach ($permissions as $key => $permission){
            $module = Module::find($permission->module_id);
            if($module->id == $permission->module_id){
                $final_per[$module->name][] = $permission->name;
            }
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(10);
        $token->save();
        return HelperModule::jsonApiResponse(true, 'Successfully Logged in', [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => $user,
            'permissions' => $final_per,
        ]);
    }


    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return HelperModule::jsonApiResponse(true, Lang::get('messages.success.logout_success'), []);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $user = $request->user();
        $token = $user->token();
        return HelperModule::jsonApiResponse(true, null, [
            'user' => $user,
            'token' => $token,
        ]);
    }

    public function getUserPermissions(Request $request){
        try {
            $user = $request->user();
            $permisssion_list = HelperModule::permissionList($user);
            if(count($permisssion_list) > 0){
                return HelperModule::jsonApiResponse(200,'Permissions list', $permisssion_list);
            }
            return HelperModule::jsonApiResponse(404,'No permission exists',null);
        } catch (\Exception $ex) {
           return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
