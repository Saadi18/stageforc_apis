<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Location;
use App\LocationNote;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotesController extends Controller
{
    public function index(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required'
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $notes = LocationNote::where('location_id',$request->location_id)->get();
            $result = [];
            foreach ($notes as $index => $note){
                $result[$index] = [
                    'id'=> $note->id,
                    'note' => $note->note,
                    'note_by' => $request->user()->name.' '. $request->user()->last_name,
                    'location' => Location::find($note->location_id)->location_name,
                ];
            }
            return HelperModule::jsonApiResponse(200,'Note assigned list',$result);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function create(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required',
                'note' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $note_by = $request->user()->name.' '.$request->user()->last_name;
            $data = $request->all();
            $data['note_by'] = $note_by;
            $note = LocationNote::create($data);
            $result = [];
            if($note){
                $result[] = [
                    'id'=> $note->id,
                    'note' => $note->note,
                    'note_by' => $note_by,
                    'location' => Location::find($note->location_id)->location_name,
                ];
                return HelperModule::jsonApiResponse(200,'Note assigned',$result);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function edit(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $note = LocationNote::find($request->id);
            if($note){
                return HelperModule::jsonApiResponse(200,'Note detail',$note);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function update(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'id' => 'required',
                'note' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $note_by = $request->user()->name.' '.$request->user()->last_name;
            $data = $request->all();
            $data['note_by'] = $note_by;
            $obj = LocationNote::find($request->id);
            $obj->update($data);
            return HelperModule::jsonApiResponse(200,'Note update successfully', $obj);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function delete(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $obj = LocationNote::find($request->id);
            $obj->delete();
            return HelperModule::jsonApiResponse(200,'Note deleted successfully');
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

}
