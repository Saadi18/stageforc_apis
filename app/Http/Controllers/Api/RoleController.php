<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Models\Module;
use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;
use Spatie\Permission\Models\Role;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $roles = Role::with('permissions')->get();
            return response()->json([
                'status' => 200,
                'message' => 'Role listing',
                'data' => RoleResource::collection($roles),
            ]);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500, $ex->getMessage(), null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($request->name == '')
                return HelperModule::jsonApiResponse(422, 'Role name is required', null);
            $data['name'] = $request->name;
            $role = Role::create($data);
            $permission = $request->permissions;
            $role->givePermissionTo($permission);
            return response()->json([
                'status' => 200,
                'message' => 'Role created successfully',
                'data' => new RoleResource($role),
            ]);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500, $ex->getMessage() . '' . $ex->getLine(), null);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function showRoleDetai(Request $request){
        try {
            $role = Role::with('permissions')->find($request->id);
            if($role)
                return HelperModule::jsonApiResponse(200, 'Role detial', $role);

            return HelperModule::jsonApiResponse(422, 'Role not exists');
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500, $ex->getMessage() . '' . $ex->getLine(), null);
        }
    }
    public function show($id)
    {
        try {
            $role = Role::with('permissions')->find($id);
            if ($role){
                $permissions = Permission::all();
                $result = [];
                foreach ($permissions as $key => $per) {
                    $module = Module::find($per->module_id);
                    $result[$key]['moduleName'] = $module->name;
                    $check_permission = DB::table('role_has_permissions')->where('permission_id', $per->id)->exists();
                    $result[$key]['permissions'] = [
                        'permissionId' => $per->id,
                        'module_id' => $per->module_id,
                        'permissionName' => $per->name,
                        'status' => $check_permission,
                    ];
                }
                $final_array = array();
                $per_array = array();
                foreach ($result as $index => $item) {
                    if (!in_array($item['moduleName'], $per_array)) {
                        $per_array[] = $item['moduleName'];

                        $final_array[$item['moduleName']] = [
                            'moduleName' => $item['moduleName'],
                            'permissions' => [],
                        ];
                    }
                    $final_array[$item['moduleName']]['permissions'][] = $item['permissions'];
                }
                return HelperModule::jsonApiResponse(true, 'Role detail', [
                    'role_detail' => $role,
                    'permissions_detail' => $final_array,
                ]);
            }
            return HelperModule::jsonApiResponse(200, 'Role not exists');
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500, $ex->getMessage() . '' . $ex->getLine(), null);
        }

//        try{
//            $role = Role::find($id);
//            if($role){
//                $permissions = Permission::all();
//                $result = [];
//                foreach ($permissions as $key => $per){
//                    $module = Module::find($per->module_id);
////                    if ($module->id == $per->module_id) {
//                        $result[$key]['moduleName'] = $module->name;
//                        $check_permission = DB::table('role_has_permissions')->where('permission_id',$per->id)->exists();
//                        $result[$key]['permissions'] = [
//                            'permissionId' => $per->id,
//                            'module_id' => $per->module_id,
//                            'permissionName' => $per->name,
//                            'status' => $check_permission,
//                        ];
////                    }
//                }
//                $final_array = array();
//                $per_array = array();
//                foreach ($result as $index => $item){
//                    if(!in_array($item['moduleName'], $per_array)){
//                        $per_array[] = $item['moduleName'];
//
//                        $final_array[$item['moduleName']] = [
//                            'moduleName' => $item['moduleName'],
//                            'permissions' => [],
//                        ];
//                    }
//                    $final_array[$item['moduleName']]['permissions'][] = $item['permissions'];
//                }
//                return HelperModule::jsonApiResponse(true, 'Role found', [
//                    'role_detail' => $role,
//                    'permissions_detail' => $final_array,
//                ]);
//            }
//
//        return HelperModule::jsonApiResponse(200,'Role not exists');
//        }catch (\Exception $ex){
//            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);
//        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $permission = $request->permissions;
            $roles = Role::find($id);
            $role = $roles->update($request->except('permissions'));
            $roles->syncPermissions($permission);
            return HelperModule::jsonApiResponse(200, 'Role Updated', $roles);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500, $ex->getMessage() . '' . $ex->getLine(), null);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
