<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $categories = Category::paginate(config('pagination'));
            $pagination = [];
            $pagination['perpage'] = $categories->perPage();
            $pagination['nextPageUrl'] = $categories->nextPageUrl();
            $nextPageNumber = explode('=', $categories->nextPageUrl());
            $pagination['nextpage'] = isset( $nextPageNumber[1] ) ? intval($nextPageNumber[1]) : $categories->nextPageUrl() ;
            $pagination['previouspage'] = $categories->previousPageUrl();
            $pagination['currentPage'] =$categories->currentPage();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Categories listing',
                'vendors'        => CategoryResource::collection($categories),
                'pagination'        => $pagination,
                'permissions'        => HelperModule::permissionList(Auth::user())
            ]);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'category_name'    =>  'required',
                'category_descripition'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }
            }
            $form_data = array(
                'category_name' => $request->category_name,
                'category_descripition' => $request->category_descripition,
            );
            if(empty($request->parent_category) || $request->parent_category == 'undefined'){
                $form_data['category_id'] = null;
            }else{
                $form_data['category_id'] =  $request->parent_category;
            }

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/category_images');
                $image->move($destinationPath, $name);
                $form_data['category_image'] = $name;
            }
//            dd($form_data);
            $category = Category::create($form_data);
            if($category){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Category Stored Successfully',
                    'Category'        => new CategoryResource($category),
                ]);
            }
        }catch (\Exception $ex){
           return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $category = Category::find($id);
            if($category)
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Category found',
                    'category'        => new CategoryResource($category),
                ]);

            return HelperModule::jsonApiResponse(404,'category Not Exist',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'category_name' => 'required',
                'category_descripition' => 'required|max:1000',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $category = Category::find($request->id);
            $category->category_name = $request->category_name;
            $category->category_descripition = $request->category_descripition;
            if($request->parent_category != null && $request->parent_category != 'undefined'){
                $category->category_id =  $request->parent_category;
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/category_images');
                $image->move($destinationPath, $name);
                $category->category_image = $name;
            }
            $category->update();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Category updated Successfully',
                'Category'        => new CategoryResource($category),
            ]);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $category = Category::find($id);
            if($category){
                if($category->childrenCategories()->count() > 0){
                   return HelperModule::jsonApiResponse(422,'Category Contains child cannot be deleted',null);
                }else{
                    $category->delete();
                    return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
                }
            }
            return HelperModule::jsonApiResponse(200,'No record found',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function parentChildCategoriesData(){
        $categories = Category::whereNull('category_id')->with('childrenCategories')->get();
        $result_Array = [];
        foreach ($categories as $index => $category){
            $result_Array[$index]['id'] = $category->id;
            $result_Array[$index]['text'] = $category->category_name;
            $result_Array[$index]['expanded'] = true;
            if(count($category->childrenCategories) > 0){
                foreach ($category->childrenCategories as $key => $child){
                    $result_Array[$index]['items'][$key]['id'] = $child->id;
                    $result_Array[$index]['items'][$key]['text'] = $child->category_name;
                    $result_Array[$index]['items'][$key]['expanded'] = true;
                    if(count($child->categories) > 0 ){
                        foreach ($child->categories as $subIndex => $subChild){
                            $result_Array[$index]['items'][$key]['items']['id'] = $subChild->id;
                              $result_Array[$index]['items'][$key]['items']['text'] = $subChild->category_name;
                            $result_Array[$index]['items'][$key]['items']['expanded'] = true;
                        }
                    }
                }
            }
        }
        return $result_Array;
    }

    public function parentChildCats(){
        try {
            $categories = Category::whereNull('category_id')->with('childrenCategories')->get();
            $result_Array = [];
            foreach ($categories as $index => $category) {
                $result_Array[$index]['id'] = $category->id;
                $result_Array[$index]['text'] = $category->category_name;
                $result_Array[$index]['expanded'] = false;
                if (count($category->childrenCategories) > 0) {
                    $result_Array[$index]['expanded'] = true;
                    foreach ($category->childrenCategories as $key => $child) {
                        $result_Array[$index]['items'][$key]['id'] = $child->id;
                        $result_Array[$index]['items'][$key]['text'] = $child->category_name;
                        $result_Array[$index]['items'][$key]['expanded'] = true;
                        if (count($child->categories) > 0) {
                            foreach ($child->categories as $subIndex => $subChild) {
                                $result_Array[$index]['items'][$key]['items']['id'] = $subChild->id;
                                $result_Array[$index]['items'][$key]['items']['text'] = $subChild->category_name;
                                $result_Array[$index]['items'][$key]['items']['expanded'] = true;
                            }
                        }
                    }
                }
            }
            return response()->json([
                'status'      =>  200,
                'message'     => 'Parent Child level',
                'Category'        => $result_Array,
            ]);
        } catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function getParentCats(Request $request){
        try {
            //getting all parent categories to show in api
            $categories = Category::whereNull('category_id')->get();
            if(isset($request->product_id)){
                // this chunk is to exclude the parent categoreis that are already assigned to the specific product
                $prdouct = Product::find($request->product_id);
                $assign_categoreis_id = $prdouct->assignedCategories()
                    ->groupBy('parent_category_id')->pluck('parent_category_id');
                $categories = $categories->whereNotIn('id',$assign_categoreis_id);
//                $categories = collect($categories);
                if(count($categories) == 0){
                   return HelperModule::jsonApiResponse(200,'All parent categories assigned to this product',null);
                };
            }
            $result_Array = [];
            foreach ($categories as $index => $category){
                $result_Array[] = [
                    'id' => $category->id,
                    'name' => $category->category_name,
                ];
            }
            return response()->json([
                'status'      =>  200,
                'message'     => 'Parent Categories listing',
                'Categories'        => $result_Array,
            ]);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function getCatsData(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'product_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors,null);
            }
            $prodcut = Product::find($request->product_id);
            if($prodcut){
                if($prodcut->getAssignedParentCategories){
                    $assig_parent_cats = $prodcut->getAssignedParentCategories()
                        ->with('parentCategory')->groupBy('parent_category_id')->get();
                    if($prodcut->getAssignedChildCategories){
                        $assig_child_cats = $prodcut->getAssignedChildCategories()->with('childCategory')->get();
                        $assign_child_cat_ids = $assig_child_cats->pluck('child_category_id');
                        $child_cats = Category::getChildCategories()->whereNotIN('id',$assign_child_cat_ids)->toArray();
                    }
                }
                $result = array();
                if(isset($assig_parent_cats) & count($assig_parent_cats) > 0){
                    foreach ($assig_parent_cats as $key => $cat){
                        $result['assigned_parent_cats'][$key] = [
                            'id' => $cat->parentCategory->id,
                            'name' => $cat->parentCategory->category_name,
                            'child_cats_assigned' => array(),
                            'child_cats_exculded_assgined' => array(),
                        ];
                        if(count($assig_child_cats) > 0){
                            foreach ($assig_child_cats as $child_index => $child){
                                if($cat->parentCategory->id == $child->parentCategory->id){
                                    $result['assigned_parent_cats'][$key]['child_cats_assigned'][] = [
                                        'id' => $child->childCategory->id,
                                        'name' => $child->childCategory->category_name,
                                    ];
                                }
                            }
                        }
                        if(count($child_cats) > 0){
                            foreach ($child_cats as $cat_index => $child_cat){
                                if($cat->parentCategory->id == $child_cat['category_id']){
                                    $result['assigned_parent_cats'][$key]['child_cats_exculded_assgined'][] = [
                                        'id' => $child_cat['id'],
                                        'name' => $child_cat['category_name'],
                                    ];
                                }
                            }
                        }else{
                            $result['assigned_parent_cats'][$key]['child_cats_exculded_assgined'] = null;
                        }
                    }
                    return response()->json([
                        'status'      =>  200,
                        'message'     => 'Categories listing',
                        'Categories'        => $result,
                    ]);
                }else{
                    return response()->json([
                        'status'      =>  404,
                        'message'     => 'No categories assigned',
                        'Categories'        => $result,
                    ]);
                }
            }else{
                return HelperModule::jsonApiResponse(422,'Product not found',null);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage().'-'. $ex->getFile().'-'.$ex->getLine(),null);
        }
    }

    public function deleteAssignedCat(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'product_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors,null);
            }
            if(isset($request->product_id) && isset($request->parent_category_id) && !isset($request->child_category_id)){
                $objects = ProductCategory::where('product_id', $request->product_id)
                    ->where('parent_category_id',$request->parent_category_id)->get();
                if(count($objects)){
                    foreach ($objects as $obj){
                        $obj->delete();
                    }
                    return HelperModule::jsonApiResponse('200','Parent category with all assigned child successfully deleted',null);
                }
            }

            if(isset($request->product_id) && isset($request->parent_category_id) && isset($request->child_category_id)){
                $object = ProductCategory::where('product_id', $request->product_id)
                    ->where('parent_category_id',$request->parent_category_id)
                    ->where('child_category_id',$request->child_category_id)->first();
                if($object->delete()){
                    return HelperModule::jsonApiResponse('200','Child category successfully deleted',null);
                }
            }


        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
