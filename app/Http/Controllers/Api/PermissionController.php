<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $permissions = Permission::with('module')->get();
               return HelperModule::jsonApiResponse(200,'Permission Listing',$permissions);
        }catch (\Exception $exception){
            return HelperModule::jsonApiResponse(500,$exception->getMessage().''.$exception->getLine(),null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'module_id' => 'required',
                'name' => 'required',
            ]);
            $errors = $validator->errors();
            $error = implode(',', $errors->all());
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$error);
            }
            $data['name'] = $request->name;
            $data['slug'] = $request->name;
            $data['module_id'] = $request->module_id;
            $permission = Permission::create($data);
            return HelperModule::jsonApiResponse(200,'Permission created Successfully',$permission);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $role = Permission::with('roles')->find($id);
            if($role)
                return HelperModule::jsonApiResponse(200,'Permission detail',$role);

            return HelperModule::jsonApiResponse(200,'Permission not exists');
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage().''.$ex->getLine(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
