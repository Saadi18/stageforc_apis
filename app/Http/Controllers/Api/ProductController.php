<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductCategoryResource;
use App\Http\Resources\ProductQuantityResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductResourceCollection;
use App\Models\ProductQuantity;
use App\Product;
use App\ProductCategory;
use App\ProductFlag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Models\PrdouctRfid;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:product_view'|| auth::user()->roles[0]->name =='super-admin')->only('index');
        $this->middleware('permission:product_create' || auth::user()->roles[0]->name =='super-admin', ['only' => ['create']] );
        $this->middleware('permission:product_edit' || auth::user()->roles[0]->name =='super-admin', ['only' => ['create']] );
//        $this->middleware('permission:notification-delete' || auth::user()->roles[0]->name =='super-admin', ['only' => ['delete']] );
//        $this->middleware('permission:notification-devices' || auth::user()->roles[0]->name =='super-admin', ['only' => ['devices']] );
    }

    public function index()
    {
        try{
            $products = Product::paginate(config('pagination'));
            $pagination = [];
            $pagination['perpage'] = $products->perPage();
            $pagination['nextPageUrl'] = $products->nextPageUrl();
            $nextPageNumber = explode('=', $products->nextPageUrl());
            $pagination['nextpage'] = isset( $nextPageNumber[1] ) ? intval($nextPageNumber[1]) : $products->nextPageUrl() ;
            $pagination['previouspage'] = $products->previousPageUrl();
            $pagination['currentPage'] = $products->currentPage();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Product listing',
                'products'        => ProductResource::collection($products),
                'pagination'        => $pagination
            ]);
        }catch (\Exception $ex){
           return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $product_validation = new ProductRequest();
            $rules = $product_validation->rules();
            $validator = Validator::make($request->all(),$rules,$product_validation->messages());
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse(422, $validator->errors());
            }
            if ($request->hasFile('image') ) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }

            }
            $data = $request->all();
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/product_images');
                $image->move($destinationPath, $name);
                $data['image'] = $name;
            }
            $product = Product::create($data);
            if($product){
                $res =[];
                $res['quantity'] = $request->quantity;
                $res['product_id'] = $product->id;
                $product_quantity = ProductQuantity::create($res);
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Product Stored Successfully',
                    'products'        => new ProductResource($product),
                ]);
            }else{
                return response()->json(['status' => 422, 'message' => 'something went wrong', 'data' => $request->all()]);
            }
        }catch (\Exception $ex){
            dd($ex->getMessage(), $ex->getFile());
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $product = Product::find($id);
            if($product)
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Product Stored Successfully',
                    'products'        => new ProductResource($product),
                ]);

            return HelperModule::jsonApiResponse(404,'Product Not Exist',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $product_validation = new ProductRequest();
            $rules = $product_validation->updateRules();
            $validator = Validator::make($request->all(),$rules,$product_validation->messages());
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse(422, $validator->errors(),null);
            }
            $product = Product::find($request->id);
            if($product == null)
                return HelperModule::jsonApiResponse(404, 'No product exists with this id',null);

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'),null);
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'),null,null);
                }

            }
            $image_name = '';
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/product_images');
                $image->move($destinationPath, $name);
                $image_name = $name;
            }
            $product->product_name = $request->product_name;
            $product->product_description = $request->product_description;
            if(isset( $request->sku)){
                $product->sku = $request->sku;
            }else{
                $product->sku = '';
            }
            if(isset( $request->vendor)){
                $product->vendor = $request->vendor;
            }else{
                $product->vendor = '';
            }
            if(isset($request->length)){
                $product->length = $request->length;
            }else{
                $product->length = '';
            }
            if(isset($request->width)){
                $product->width = $request->width;
            }else{
                $product->width = '';
            }
            if(isset($request->height)){
                $product->height = $request->height;
            }else{
                $product->height = '';
            }
            if(isset($request->purchase_date)){
                $product->purchase_date = $request->purchase_date;
            }
            if(isset($request->purchase_price)){
                $product->purchase_price = $request->purchase_price;
            }
//            if(isset($request->rental_price)){
//                $product->rental_price = $request->rental_price;
//            }else{
//                $product->rental_price = null;
//            }
//            if(isset($request->sale_price)){
//                $product->sale_price = $request->sale_price;
//            }else{
//                $product->sale_price =null;
//            }
//            if(isset($request->working_value)){
//                $product->working_value = $request->working_value;
//            }else{
//                $product->working_value = null;
//            }
            if(isset($image_name)){
                $product->image = $image_name;
            }
            $product->update();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Product updated Successfully',
                'products'        => new ProductResource($product),
            ]);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $product = Product::find($id);
            $product->delete();
            return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }

    }

    public function searchProducts(Request $request){
        try{
            $name = $request->product_name;
            $products = Product::where('product_name','like', '%'.$name.'%')->get();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Search Result',
                'products'        => ProductResource::collection($products),
            ]);
        }catch (\Exception $ex){
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
    public function addMoreQuantity(Request $request){
        try {
            $quantity = ProductQuantity::create($request->all());
            if($quantity){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Product Quantity Result',
                    'products'        => new ProductQuantityResource($quantity),
                ]);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function getProductQuantity(Request $request){
        $product_id = $request->product_id;
        $quantity = ProductQuantity::where('product_id',$product_id)->get();
        if($quantity){
            return response()->json([
                'status'      =>  200,
                'message'     => 'Product Quantity Result',
                'products'        => ProductQuantityResource::collection($quantity),
            ]);
        }
    }
    public function assignCategory(Request $request)
    {
        try {
            $product_id = $request->product_id;
            if(isset($request->parent_category_id) && $request->parent_category_id != null
                && !isset($request->child_category_id)){
                $check_parnet_existance = ProductCategory::where('product_id',$product_id)
                    ->where('parent_category_id',$request->parent_category_id)->first();
                if( $check_parnet_existance == null && !isset($request->child_category_id)){
                    $obj = new ProductCategory();
                    $obj->product_id = $product_id;
                    $obj->parent_category_id = $request->parent_category_id;
                    if ($obj->save()) {
                        $obj['saving_parent_cat'] = 1;
                        $obj['saving_child_cat'] = 0;
                        return response()->json([
                            'status'      =>  200,
                            'message'     => 'Parent Category Assigned',
                            'Category'        => new ProductCategoryResource($obj),
                        ]);
                    }
                }else{
                    return HelperModule::jsonApiResponse(422,'Parent Category already assigned',null);
                }
            }
            if(isset($request->child_category_id) && $request->child_category_id != null){
                $check_child_existance = ProductCategory::where('product_id',$product_id)
                    ->where('parent_category_id',$request->parent_category_id)
                    ->where('child_category_id',$request->child_category_id)
                    ->first();
                if($check_child_existance == null && isset($request->child_category_id)){
                    $obj = new ProductCategory();
                    $obj->product_id = $product_id;
                    $obj->parent_category_id = $request->parent_category_id;
                    $obj->child_category_id = $request->child_category_id;
                    if ($obj->save()) {
                        $obj['saving_child_cat'] = 1;

                        $obj['saving_parent_cat'] = 0;
                        return response()->json([
                            'status'      =>  200,
                            'message'     => 'Child Category Assigned',
                            'Category'        => new ProductCategoryResource($obj),
                        ]);
                    }
                }else{
                    return HelperModule::jsonApiResponse(422,'Child Category already assigned',null);
                }
            }


        } catch (\Exception $exception) {
            return HelperModule::jsonApiResponse(500,$exception->getMessage() . '--' . $exception->getCode() . '--' . $exception->getFile(),null);
        }
    }

    public function getAssignedCategories(Request $request){
       $product = Product::find($request->product_id);
       $cats = $product->categories;
        if ($cats) {
            return response()->json([
                'status'      =>  200,
                'message'     => 'Assigned Category list',
                'Category'        => $cats,
            ]);
        }
    }

    public function assignRfids(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'product_id'    =>  'required',
                'rfid'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $check = PrdouctRfid::where('product_id',$request->product_id)->where('rfid',$request->rfid)->first();
            if($check){
                return HelperModule::jsonApiResponse('422','Rfid already assigned to this product',null);
            }
            $obj = PrdouctRfid::create($request->all());
            if($obj){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Rfid assigned',
                    'Rfid'        => $obj,
                ]);
            }

        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$exception->getMessage() . '--' . $exception->getCode() . '--' . $exception->getFile(),null);

        }
    }

    public function getAllRfids(Request $request){
        try {
            $rfids = PrdouctRfid::where('product_id',$request->product_id)->get()->toArray();
            if($rfids){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Rfids list',
                    'Rfid'        => $rfids,
                ]);
            }
            return response()->json([
                'status'      =>  200,
                'message'     => 'No rfid exists against this product',
                'Rfid'        => null,
            ]);
        } catch (\Exception $exception) {
            return HelperModule::jsonApiResponse(500,$exception->getMessage() . '--' . $exception->getCode() . '--' . $exception->getFile(),null);
        }
    }

    public function assignProductFlag(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'product_id'    =>  'required',
                'rfid'     =>  'required',
                'issue_type'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $check_product_rfid = PrdouctRfid::where('product_id',$request->product_id)->where('rfid',$request->rfid)->exists();
            if(!$check_product_rfid){
                return HelperModule::jsonApiResponse('422','Rfid not assigned to this product',null);

            }
            $check = ProductFlag::where('product_id',$request->product_id)->where('rfid',$request->rfid)
                ->where('issue_type',$request->issue_type)->first();
            if($check){
                return HelperModule::jsonApiResponse('422','Issue type already assigned to this product  with this rfid',null);
            }
            $obj = ProductFlag::create($request->all());
            if($obj){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Flag assigned',
                    'Rfid'        => $obj,
                ]);
            }

        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage() . '--' . $ex->getCode() . '--' . $ex->getFile(),null);

        }
    }

    public static function getAllFlags(Request $request){
        try {
            $flags = ProductFlag::where('product_id',$request->product_id)->get();
            if($flags){
                $result = [];
                foreach ($flags as $key => $flag) {
                    $result[$key]['id'] = $flag->id;
                    $result[$key]['product_id'] = $flag->product_id;
                    $result[$key]['issue_type'] = $flag->issue_type;
                    $result[$key]['rfid'] = $flag->rfid ;
                    $result[$key]['created_at'] = HelperModule::dateFormat($flag->created_at);
                    $result[$key]['updated_at'] = $flag->updated_at;
                    $result[$key]['deleted_at'] = $flag->deleted_at;
                }
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Flags list',
                    'Rfid'        => $result,
                ]);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage() . '--' . $ex->getCode() . '--' . $ex->getFile(),null);
        }
    }

    public function sortProduct(Request $request){
        try {
            $sort_by = isset($request->sort_by) ? $request->sort_by : '';
            $sort_by_date = isset($request->sort_by_date) ? $request->sort_by_date : '';

            if($sort_by == 'name'){
                $products = Product::orderBy('product_name','ASC')->get();
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Search Result',
                    'products'        => ProductResource::collection($products),
                ]);
            }
            if($sort_by_date == 'asc'){
                $products = Product::orderBy('created_at','asc')->get();
//                    dd($products);
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Search Result',
                    'products'        => ProductResource::collection($products),
                ]);
            }
            if($sort_by_date == 'desc'){
                $products = Product::orderBy('created_at','desc')->get();
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Search Result',
                    'products'        => ProductResource::collection($products),
                ]);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage() . '--' . $ex->getCode() . '--' . $ex->getFile(),null);
        }
    }

    public function deleteRfid(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'product_id'    =>  'required',
                'rfid'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $obj = PrdouctRfid::where('product_id',$request->product_id)->where('rfid',$request->rfid)->first();
            if($obj){
                $delete = $obj->delete();
                return HelperModule::jsonApiResponse(200,'Rfid delted',null);
            }else{
                return HelperModule::jsonApiResponse(404,'Rfid not exists',null);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage() . '--' . $ex->getCode() . '--' . $ex->getFile(),null);
        }
    }

    public function deleteFlag(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'id'    =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $obj = ProductFlag::find($request->id);
            if($obj){
                $delete = $obj->delete();
                return HelperModule::jsonApiResponse(200,'Flag delted',null);
            }else{
                return HelperModule::jsonApiResponse(404,'Flag not exists',null);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage() . '--' . $ex->getCode() . '--' . $ex->getFile(),null);
        }
    }

    public function getNotAssginedRfids(){
        try {
            $rfids = PrdouctRfid::all()->pluck('rfid')->toArray();

            $data_array = $rfids;
            $total = 1000;
            $new_array =[];
            for ($i = 1; $i<=$total; $i++){
                if(!in_array($i, $data_array)){
                    $new_array[] = $i;
                }
            }
            return response()->json(['status'=> 200, 'message' =>'All not assigned rfids', 'data'=>$new_array]);
        } catch (\Exception $ex) {

        }
    }

}
