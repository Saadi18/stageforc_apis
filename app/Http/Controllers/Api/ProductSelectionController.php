<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\PrdouctRfid;
use App\Product;
use App\ProductSelection;
use App\ProductsLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductSelectionController extends Controller
{
    public function productSelection(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'product_id' => 'required',
                'location_id' => 'required',
//                'product_rfid' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors,null);
            }
            $product_rfid = PrdouctRfid::where('product_id',$request->product_id)->first();
            if($product_rfid == null)
                return HelperModule::jsonApiResponse(422,'No Rfid assigned to this product',null);
            $check = ProductSelection::where('product_id',$request->product_id)
                ->where('product_rfid',$product_rfid->rfid)
                ->where('location_id',$request->location_id)
                ->first();
            if($check){
                $check->delete();
                return HelperModule::jsonApiResponse(200,'Product removed from selection',null);
            }
            $data = $request->all();
            $data['status'] = 'active';
            $data['product_rfid'] = $product_rfid->rfid;
            $obj= ProductSelection::create($data);
            if($obj){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Product selected',
                    'product_selection' => $obj,
                    'prdocut' => Product::find($request->product_id),
                ]);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function removeSelection(Request $request)
    {
        try {
            $validator = Validator::make($request->all(),[
//                'product_id' => 'required',
                'location_id' => 'required',
//                'product_rfid' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if(isset($request->product_id)){
                $product_rfid = PrdouctRfid::where('product_id',$request->product_id)->first();
                if($product_rfid == null)
                    return HelperModule::jsonApiResponse(422,'No Rfid assigned to this product',null);
                $obj = ProductSelection::where('product_id',$request->product_id)
                    ->where('product_rfid',$product_rfid->rfid)
                    ->where('location_id',$request->location_id)->first();
                if($obj){
                    $obj->delete();
                    return HelperModule::jsonApiResponse(200,'Product deleted from location',null);
                }
            }else{
                $obj = ProductSelection::where('location_id',$request->location_id)->get();
                foreach ($obj as $key => $ob){
                    $ob->delete();
                }
                return HelperModule::jsonApiResponse(200,'Cart Empty',null);

            }

        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function pauseSelection(Request $request)
    {
        try {
//            $validator = Validator::make($request->all(),[
//                'location_id' => 'required',
////                'status' => 'required',
//            ]);
//            $errors = $validator->errors();
//            if ($validator->fails()) {
//                return HelperModule::jsonApiResponse('422',$errors);
//            }
            if($request->location_id){
                $update_status = ProductSelection::where('location_id',$request->location_id)->update(array('status' => 'paused'));
                if($update_status){
                    $update_status = ProductSelection::where('location_id',$request->location_id)->get();
                    return HelperModule::jsonApiResponse(200,'Status updated',$update_status);
                }
            }else{
                $update_status = ProductSelection::where('status','active')->update(array('status' => 'paused'));
                if($update_status){
                    $update_status = ProductSelection::where('status','paused')->get();
                    return HelperModule::jsonApiResponse(200,'Status updated',$update_status);
                }
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function resumeSelection(Request $request)
    {
        try {
//            $validator = Validator::make($request->all(),[
//                'location_id' => 'required',
////                'status' => 'required',
//            ]);
//            $errors = $validator->errors();
//            if ($validator->fails()) {
//                return HelperModule::jsonApiResponse('422',$errors);
//            }
            if($request->location_id){
                $update_status = ProductSelection::where('location_id',$request->location_id)->update(array('status' => 'active'));
                if($update_status){
                    $update_status = ProductSelection::where('location_id',$request->location_id)->get();
                    $location_id = isset($update_status[0]) ? $update_status[0]['location_id'] : 'null';
                    return response()->json(['status'=>200, 'message'=> 'Status updated',
                        'data'=> $update_status,'location_id'=> $location_id]);
                }
            }else{
                $update_status = ProductSelection::where('status','paused')->update(array('status' => 'active'));
                $data = ProductSelection::where('status','active')->get();
                if($data){
                    $update_status = ProductSelection::where('status','active')->get();
                    $location_id = isset($update_status[0]) ? $update_status[0]['location_id'] : 'null';
                    return response()->json(['status'=>200, 'message'=> 'Status updated',
                        'data'=> $update_status,'location_id'=> $location_id]);
                }
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function finishSelection(Request $request)
    {
        try {
//            $validator = Validator::make($request->all(),[
//                'location_id' => 'required',
////                'status' => 'required',
//            ]);
//            $errors = $validator->errors();
//            if ($validator->fails()) {
//                return HelperModule::jsonApiResponse('422',$errors);
//            }
            if($request->location_id){
                $data = ProductSelection::where('location_id',$request->location_id)->get();
            }else{
                $data = ProductSelection::all();
            }
            $result=[];
            if(count($data)){
                foreach ($data as $key => $val){
                    $assig_location = new ProductsLocation();
                    $assig_location->product_id = $val->product_id;
                    $assig_location->product_rfid = $val->product_rfid;
                    $assig_location->location_id = $val->location_id;
                    $assig_location->status = 'selected';

                    $result[$key] = [
                        'id' => $assig_location->id,
                        'product_id' => $val->product_id,
                        'location_id' =>$val->location_id,
                        'status' => 'selected',
                    ];
                    $assig_location->save();
                    $val->delete();
                }
            }
//            $result = ProductsLocation::where('location_id',$request->location_id)->where('status','selected')->get();
//            return HelperModule::jsonApiResponse(200,'Assigned',$result);
            $location_id = isset($data[0]) ? $data[0]['location_id'] : 'null';
            return response()->json(['status'=>200, 'message'=> 'Assigned','data'=> $result,'location_id'=> $location_id]);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function getAllSelectedProducts(Request $request){
        try {
            if(isset($request->location_id)){
                $data = ProductSelection::where('location_id',$request->location_id)->get();
            }else{
                $data = ProductSelection::all();
            }
            $result = [];
            if(count($data)){
                foreach ($data as $key => $val){
                    $product = Product::find($val->product_id);
                    if($product->image == null || $product->image == 'undefined'){
                        $product_image = null;
                    }else{
                        $product_image = url('/product_images').'/'.$product->image;
                    }
                    $result[$key]['id'] = $val->id;
                    $result[$key]['product_id'] = $val->product_id;
                    $result[$key]['location_id'] = $val->location_id;
                    $result[$key]['product_rfid'] = $val->product_rfid;
                    $result[$key]['status'] = $val->status;
                    $result[$key]['product_name'] = $product->product_name;
                    $result[$key]['product_image'] = $product_image;
                    $result[$key]['parent_cat_name'] = $product->getAssignedParentCategories()->first()->parentCategory ? $product->getAssignedParentCategories()->first()->parentCategory->category_name : '';
                }
            }
            $status = isset($result[0]) ? $result[0]['status'] : 'null';
            return response()->json(['status'=>200, 'message'=> 'selected products lists','data'=> $result,'selection_status'=> $status]);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function markeLocated(Request $request){
        dd($request->all());
    }
}
