<?php

namespace App\Http\Controllers\Api;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Http\Resources\LocationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Location;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $categories = Location::paginate(config('pagination'));
            $pagination = [];
            $pagination['perpage'] = $categories->perPage();
            $pagination['nextPageUrl'] = $categories->nextPageUrl();
            $nextPageNumber = explode('=', $categories->nextPageUrl());
            $pagination['nextpage'] = isset( $nextPageNumber[1] ) ? intval($nextPageNumber[1]) : $categories->nextPageUrl() ;
            $pagination['previouspage'] = $categories->previousPageUrl();
            $pagination['currentPage'] =$categories->currentPage();
            return response()->json([
                'status'      =>  200,
                'message'     => 'Location listing',
                'Locations'        => LocationResource::collection($categories),
                'pagination'        => $pagination
            ]);
        }catch (\Exception $ex){
            dd($ex->getLine(), $ex->getMessage(), $ex->getFile(), $ex->getCode());
            HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'location_name'    =>  'required',
                'location_description'     =>  'required',
                'street_address'    =>  'required',
                'city'    =>  'required',
//            'product'    =>  'required',
                'state'    =>  'required',
                'zip_code'     =>  'required',
                'home_status'    =>  'required',
                'access_code'     =>  'required',
                'data_of_stage'    =>  'required',
                'date_of_destage'     =>  'required',
                'location_square_feet'    =>  'required',
                'price_estimate'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'));
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'));
                }
            }
            $data = $request->all();
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/location_images');
                $image->move($destinationPath, $name);
                $data['image'] = $name;
            }
            $location = Location::create($data);
            if($location){
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Location Stored Successfully',
                    'Location'    => new LocationResource($location),
                ]);
            }

        }catch (\Exception $ex){
            dd($ex->getLine(), $ex->getMessage(), $ex->getFile(), $ex->getCode());
//            HelperModule::jsonApiResponse(500,$ex->getLine().','.$ex->getMessage(),null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $category = Location::find($id);
            if($category)
                return response()->json([
                    'status'      =>  200,
                    'message'     => 'Location found',
                    'Location'        => new LocationResource($category),
                ]);

            return HelperModule::jsonApiResponse(404,'Location Not Exist',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'location_name'    =>  'required',
                'location_description'     =>  'required',
                'street_address'    =>  'required',
                'city'    =>  'required',
//            'product'    =>  'required',
                'state'    =>  'required',
                'zip_code'     =>  'required',
                'home_status'    =>  'required',
                'access_code'     =>  'required',
                'data_of_stage'    =>  'required',
                'date_of_destage'     =>  'required',
                'location_square_feet'    =>  'required',
                'price_estimate'     =>  'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $location = Location::find($request->id);

            $image_name = '';
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/location_images');
                $image->move($destinationPath, $name);
                $image_name = $name;
            }
            $data = array(
                'location_name'       =>   $request->location_name,
                'location_description'        =>   $request->location_description,
                'product'       =>   $request->product,
                'street_address'       =>   $request->street_address,
                'city'       =>   $request->city,
                'state'      =>   $request->state,
                'zip_code'   =>   $request->zip_code,
                'home_status' =>   $request->home_status,
                'access_code'        =>   $request->access_code,
                'data_of_stage'       =>   $request->data_of_stage,
                'date_of_destage'     =>   $request->date_of_destage,
                'location_square_feet' =>   $request->location_square_feet,
                'price_estimate'   =>$request->price_estimate,
            );
            if($image_name){
                $data['image'] = $image_name;
            }
            $location->update($data);
            return response()->json([
                'status'      =>  200,
                'message'     => 'Location updated Successfully',
                'Location'        => new LocationResource($location),
            ]);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $location = Location::find($id);
            if($location){
                $location->delete();
                return HelperModule::jsonApiResponse(200,'Deleted Successfully',null);
            }
            return HelperModule::jsonApiResponse(200,'No record found',null);
        }catch (\Exception $ex){
            HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
