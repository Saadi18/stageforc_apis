<?php

namespace App\Http\Controllers\Api;

use App\AssignedRoom;
use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Room;
use App\RoomMetaData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller
{
    public function index(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required',
            ]);
            $errors = $validator->errors()->first();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $rooms = Room::where('slug','default')->get();
            $result = [];
            foreach ($rooms as $key => $room){
                if($room->image != null){
                    $room->image = url('/room_images').'/'.$room->image;
                }
                $result[$key] = [
                    'id' => $room->id,
                    'name' => $room->name,
                    'slug' => $room->slug,
                    'description' => $room->description,
                    'sq_feet' => $room->sq_feet,
                    'image' => $room->image,
                    'assigned' => $room->checkRoomAssignedToLocation($request->location_id,$room->id) ? 'Yes' : 'no',
                ];
            }
            return HelperModule::jsonApiResponse(200,'rooms list',$result);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function roomDetail(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $room = Room::find($request->id);
            if($room->image != null){
                $room->image = url('/room_images').'/'.$room->image;
            }
            return HelperModule::jsonApiResponse(200,'room  detail',$room);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function updateRoom(Request $request){
        try {
            $data = $request->all();
            $room = Room::find($request->id);
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_file_ext_with_gif'),null);
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return HelperModule::jsonApiResponse(422, Lang::get('messages.error.image_size_10_mb'),null,null);
                }

            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalName();
                $destinationPath = public_path('/room_images');
                $image->move($destinationPath, $name);
                $data['image'] = $name;
            }
            $obj = $room->update($data);
            if($obj){
                $meta = RoomMetaData::create($data);
            }
            if($meta->image != null){
                $meta->image = url('/room_images').'/'.$meta->image;
            }
            $result = [
                'id' => $room->id,
                'name' => $room->name,
            ];

            return HelperModule::jsonApiResponse(200,'room  update successfully',$room);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function create(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'location_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $room = Room::create($request->all());
            if($room){
                $assign = new AssignedRoom();
                $assign->location_id = $request->location_id;
                $assign->room_id = $room->id;
                $assign->save();
            }
            return HelperModule::jsonApiResponse(200,'room created',$room);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function assign_room(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required',
                'room_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $check = AssignedRoom::where('location_id',$request->location_id)
                ->where('room_id',$request->room_id)->first();
            if($check){
                $check->delete();
                return HelperModule::jsonApiResponse(200,'Room un-assigned',null);
            }
            $assign = AssignedRoom::create($request->all());
            if($assign){
                $detail = Room::find($request->room_id);
                return HelperModule::jsonApiResponse(200,'Assigned room detail',$detail);
            }
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }

    public function assignedRoomList(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $list = AssignedRoom::where('location_id',$request->location_id)->get();
            if(count($list)){
                $result = [];
                foreach ($list as $index => $li){
                    $room = Room::find($li->room_id);
                    $result[$index] = [
                        'id' => $li->id,
                        'location_id' =>$li->location_id,
                        'room_id' => $li->room_id,
                        'room_name' => $room->name,
                        'description' => $room->description,
                        'sq_feet' => $room->sq_feet,
                    ];
                }
                return HelperModule::jsonApiResponse(200,'Assigned rooms list',$result);
            }
            return HelperModule::jsonApiResponse(200,'Assigned rooms list',$list);
        } catch (\Exception $ex) {

        }
    }

    public function deleteAssignedRoom(Request $request){
        try {
            $validator = Validator::make($request->all(),[
                'location_id' => 'required',
                'room_id' => 'required',
            ]);
            $errors = $validator->errors();
            if ($validator->fails()) {
                return HelperModule::jsonApiResponse('422',$errors);
            }
            $obj = AssignedRoom::where('location_id',$request->location_id)
                ->where('room_id',$request->room_id)->first();
           if($obj){
               $obj->delete();
               return HelperModule::jsonApiResponse(200,'Assigned room deleted',null);
           }
            return HelperModule::jsonApiResponse(200,'No match found',null);
        } catch (\Exception $ex) {
            return HelperModule::jsonApiResponse(500,$ex->getMessage(),null);
        }
    }
}
