<?php

namespace App\Http\Resources;

use App\HelperModules\HelperModule;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductQuantityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $quantity;
    public function __construct($resource)
    {
        $this->quantity = $resource;
    }

    public function toArray($request)
    {
        return [
            'id'            => $this->quantity->id,
            'quantity'          => $this->quantity->quantity,
            'product_id'   => $this->quantity->product_id,
            'created_at'    => HelperModule::dateFormat($this->quantity->created_at),
        ];
    }
}
