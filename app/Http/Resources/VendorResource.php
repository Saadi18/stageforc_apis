<?php

namespace App\Http\Resources;

use App\HelperModules\HelperModule;
use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $vendor;
    public function __construct($resource)
    {
        $this->vendor = $resource;
    }
    public function toArray($request)
    {
        if($this->vendor->image == null ||$this->vendor->image == 'undefined' ){
            $vendor_image = null;
        }else{
            $vendor_image = url('/vendor_images').'/'.$this->vendor->image;
        }
        return [
            'id'            => $this->vendor->id,
            'name'          => $this->vendor->name,
            'description'   => $this->vendor->description,
            'image'         => $vendor_image,
            'created_at'    =>  HelperModule::dateFormat($this->vendor->created_at),
            'updated_at'    =>  HelperModule::dateFormat($this->vendor->updated_at),
        ];
    }
}
