<?php

namespace App\Http\Resources;

use App\HelperModules\HelperModule;
use App\Vendor;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $product;
    public function __construct($request)
    {
        $this->product = $request;

    }

    public function toArray($request)
    {
        if($this->product->image == null || $this->product->image == 'undefined'){
            $product_image = null;
        }else{
            $product_image = url('/product_images').'/'.$this->product->image;
        }
        return [
            'id'               => $this->product->id,
            'product_name'     => $this->product->product_name,
            'product_description'     => $this->product->product_description,
            'sku'     => $this->product->sku,
            'location_id'     => $this->product->location_id,
            'length'     => $this->product->length,
            'width'     => $this->product->width,
            'height'     => $this->product->height,
            'purchase_date'     => $this->product->purchase_date,
            'purchase_date2'     => HelperModule::dateFormat($this->product->purchase_date),
            'purchase_price'     => $this->product->purchase_price,
            'rental_price'     => $this->product->rental_price,
            'sale_price'     => $this->product->sale_price,
            'working_value'  => $this->product->working_value,
            'image'            => $product_image,
            'created_at'            =>  HelperModule::dateFormat($this->product->created_at),
            'updated_at'           =>  HelperModule::dateFormat($this->product->updated_at),
//            'categories'       => $this->product->categories,
            'quantity' => $this->product->quantity()->sum('quantity'),
            'vendor_id' => (int) $this->product->vendor,
            'vendor_name' => Vendor::find($this->product->vendor) != null ?  Vendor::find($this->product->vendor)->name : '',
        ];
    }
}
