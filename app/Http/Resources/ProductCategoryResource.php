<?php

namespace App\Http\Resources;

use App\Category;
use App\HelperModules\HelperModule;
use App\ProductCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $cat;
    public function __construct($resource)
    {
        $this->cat = $resource;
    }

    public function toArray($request)
    {
        $assigned_parent_categories = ProductCategory::getParentAssignedCategories()->pluck('parent_category_id');
        $assigned_child_categories = ProductCategory::getAssignedChildCategories()->pluck('child_category_id');
        if($this->cat->saving_parent_cat == 1){
            return [
                'id'            => $this->cat->id,
                'parent_category_id'          => $this->cat->parent_category_id,
                'parent_category_name'          => Category::find($this->cat->parent_category_id)->category_name,
                'child_category_id'          => $this->cat->child_category_id,
//                'created_at'    => HelperModule::dateFormat($this->cat->created_at),
                'child_cats_list'  => Category::getSpecificCategoryChildCategories($this->cat->parent_category_id)->whereNotIn('id',$assigned_child_categories),
//                'saving_parent_cat'  => $this->cat->saving_parent_cat,
//                'saving_child_cat'  => $this->cat->saving_child_cat,
            ];
        }elseif ($this->cat->saving_child_cat == 1){
            return [
                'id'            => $this->cat->id,
                'parent_category_id'          => $this->cat->parent_category_id,
                'parent_category_name'          => Category::find($this->cat->parent_category_id)->category_name,
                'child_category_id'          => $this->cat->child_category_id,
                'child_category_name'          => Category::find($this->cat->child_category_id)->category_name,
//                'created_at'    => HelperModule::dateFormat($this->cat->created_at),
                'updated_child_cat_list'  => Category::getSpecificCategoryChildCategories($this->cat->parent_category_id)->whereNotIn('id',$assigned_child_categories),
//                'saving_parent_cat'  => $this->cat->saving_parent_cat,
//                'saving_child_cat'  => $this->cat->saving_child_cat,
            ];
        }

    }
}
