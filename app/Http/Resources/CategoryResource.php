<?php

namespace App\Http\Resources;

use App\HelperModules\HelperModule;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $category;
    public function __construct($resource)
    {
        $this->category = $resource;
    }

    public function toArray($request)
    {
        if($this->category->category_image == null){
            $category_image = null;
        }else{
            $category_image = url('/category_images').'/'.$this->category->category_image;
        }
//        if($this->category->category_id != ''){
//            dd($this->category->getParentCategory($this->category->category_id)->category_name);
//        }
        return [
            'id'            => $this->category->id,
            'name'          => $this->category->category_name,
            'description'   => $this->category->category_descripition,
            'image'         => $category_image,
            'parent_category_name'  => $this->category->getParentCategory($this->category->category_id) != null ? $this->category->getParentCategory($this->category->category_id)   : 'N/A',
            'parent_category_id'  => $this->category->getParentCategoryId($this->category->category_id) != null ? $this->category->getParentCategoryId($this->category->category_id)   : 'null',
            'created_at'    =>  HelperModule::dateFormat($this->category->created_at),
            'updated_at'    =>  HelperModule::dateFormat($this->category->updated_at),
        ];
    }
}
