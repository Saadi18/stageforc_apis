<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $location;
    public function __construct($resource)
    {
        $this->location = $resource;
    }

    public function toArray($request)
    {
        if($this->location->image == null){
            $location_image = null;
        }else{
            $location_image = url('/location_images').'/'.$this->location->image;
        }
        return [
            'id'            => $this->location->id,
            'name'          => $this->location->location_name,
            'description'   => $this->location->location_description,
            'image'         => $location_image,
            'street_address'  => $this->location->street_address,
            'city'  => $this->location->city,
            'state'  => $this->location->state,
            'zip_code'  => $this->location->zip_code,
            'home_status'  => $this->location->home_status,
            'access_code'  => $this->location->access_code,
            'data_of_stage'  => $this->location->data_of_stage,
            'date_of_destage'  => $this->location->date_of_destage,
            'location_square_feet'  => $this->location->location_square_feet,
            'price_estimate'  => $this->location->price_estimate,
            'created_at'    =>  $this->location->created_at,
            'updated_at'    =>  $this->location->updated_at,
        ];
    }
}
