<?php

namespace App\Http\Resources;

use App\HelperModules\HelperModule;
use Illuminate\Http\Resources\Json\JsonResource;

class userResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $user;
    public function __construct($resource)
    {
        $this->user = $resource;
    }

    public function toArray($request)
    {
        if($this->user->image == null){
            $user_image = null;
        }else{
            $user_image = url('/user_images').'/'.$this->user->image;
        }
        return [
            'id'            => $this->user->id,
            'name'          => $this->user->name,
            'last_name'   => $this->user->last_name,
            'image'         => $user_image,
            'phone'  => $this->user->phone,
            'city'  => $this->user->city,
            'state'  => $this->user->state,
            'email'  => $this->user->email,
            'warehouse'  => $this->user->warehouse,
            'created_at'    =>  HelperModule::dateFormat($this->user->created_at),
            'updated_at'    =>  HelperModule::dateFormat($this->user->updated_at),
            'roles'    =>  $this->user->roles,
        ];
    }
}
