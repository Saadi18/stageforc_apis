<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrdouctRfid extends Model
{
    protected $table = 'product_rfids';
    protected $fillable = ['product_id','rfid'];
}
