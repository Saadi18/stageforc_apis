<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedRoom extends Model
{
    protected $table = 'assigned_rooms';
    protected $fillable = ['location_id','room_id'];
}
