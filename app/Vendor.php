<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vender';
    protected $fillable = ['name','description','image'];
}
