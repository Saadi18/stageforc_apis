<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsLocation extends Model
{
    protected $table = 'products_locations';
    protected $fillable = ['product_id','loction_id','product_rfid','status'];

}
