<?php

namespace App\HelperModules;
use Carbon\Carbon;

class DateTimeModule
{
    /**
     * @param $time
     * @return DateTimeModule|Carbon
     */
    static public function TimeFormat($time)
    {
        return Carbon::createFromFormat('Y-m-d G:i:s', self::CarbonObject($time)->toDateTimeString());
    }

    /**
     * @return DateTimeModule|Carbon
     */
    static public function CurrentTime()
    {
        return self::TimeFormat(self::CurrentDateTime());
    }

    /**
     * @return DateTimeModule|Carbon
     */
    static public function CurrentDateTime()
    {
        return Carbon::now();
    }
    /**
     * @param $data
     * @return DateTimeModule|Carbon
     */
    static public function CarbonObject($data)
    {
        return Carbon::parse($data);
    }
}
