<?php
/**
 * Created by PhpStorm.
 * User: rs
 * Date: 4/3/18
 * Time: 12:01 PM
 */

namespace App\HelperModules;
use Illuminate\Support\Facades\Lang;

/**
 * Class HelperData
 * @package App\HelperModules
 *
 * Note::
 * All the array data created for select option in html.
 * array("key" => "value")
 *
 * <option value = "key"> value </option>
 *
 *
 * code used to extract data
 *
 *   window.data1= "" ; document.querySelectorAll("[name=brand] option").forEach(function(e) { window.data1 += "array(\"key\" => \""+e.value + "\", \"value\" => \"" + e.text + "\") ,"; });
console.log(window.data1)
 *
 */
class HelperData
{
    /**
     * @return array
     */
    public static function status()
    {
        return array(
            array(
                "key" => "1",
                "value" => Lang::get('frontend.active')
            ) , array(
                "key" => "0",
                "value" => Lang::get('frontend.inactive')
            )
        );
    }
    /**
     * @return array
     */
    public static function currenciesList(){
        $U = '$U';
        $b = '$b';
        return array(
            array("key" => "€","value"=>"€"),
            array("key" => "؋","value"=>"؋"),
            array("key" => "Lek","value"=>"Lek"),
            array("key" => "DZD","value"=>"DZD"),
            array("key" => "Kz","value"=>"Kz"),
            array("key" => "ман","value"=>"ман"),
            array("key" => "BHD","value"=>"BHD"),
            array("key" => "BDT","value"=>"BDT"),
            array("key" => "AMD","value"=>"AMD"),
            array("key" => "BTN","value"=>"BTN"),
            array("key" => $b,"value"=>$b),
            array("key" => "KM","value"=>"KM"),
            array("key" => "P","value"=>"P"),
            array("key" => "R$","value"=>"R$"),
            array("key" => "BZ$","value"=>"BZ$"),
            array("key" => "K","value"=>"K"),
            array("key" => "BIF","value"=>"BIF"),
            array("key" => "p.","value"=>"p."),
            array("key" => "៛","value"=>"៛"),
            array("key" => "CVE","value"=>"CVE"),
            array("key" => "CFA","value"=>"CFA"),
            array("key" => "XAF","value"=>"XAF"),
            array("key" => "CLP","value"=>"CLP"),
            array("key" => "NT$","value"=>"NT$"),
            array("key" => "KMF","value"=>"KMF"),
            array("key" => "CDF","value"=>"CDF"),
            array("key" => "₡","value"=>"₡"),
            array("key" => "kn","value"=>"kn"),
            array("key" => "₱","value"=>"₱"),
            array("key" => "CYP","value"=>"CYP"),
            array("key" => "Kč","value"=>"Kč"),
            array("key" => "RD$","value"=>"RD$"),
            array("key" => "ETB","value"=>"ETB"),
            array("key" => "Nfk","value"=>"Nfk"),
            array("key" => "DJF","value"=>"DJF"),
            array("key" => "FCF","value"=>"FCF"),
            array("key" => "GEL","value"=>"GEL"),
            array("key" => "D","value"=>"D"),
            array("key" => "¢","value"=>"¢"),
            array("key" => "Q","value"=>"Q"),
            array("key" => "GNF","value"=>"GNF"),
            array("key" => "G","value"=>"G"),
            array("key" => "Ft","value"=>"Ft"),
            array("key" => "₹","value"=>"₹"),
            array("key" => "Rp","value"=>"Rp"),
            array("key" => "IQD","value"=>"IQD"),
            array("key" => "₪","value"=>"₪"),
            array("key" => "¥","value"=>"¥"),
            array("key" => "JOD","value"=>"JOD"),
            array("key" => "KES","value"=>"KES"),
            array("key" => "₩","value"=>"₩"),
            array("key" => "KWD","value"=>"KWD"),
            array("key" => "₭","value"=>"₭"),
            array("key" => "L","value"=>"L"),
            array("key" => "Ls","value"=>"Ls"),
            array("key" => "LYD","value"=>"LYD"),
            array("key" => "Lt","value"=>"Lt"),
            array("key" => "MOP","value"=>"MOP"),
            array("key" => "MGA","value"=>"MGA"),
            array("key" => "MK","value"=>"MK"),
            array("key" => "RM","value"=>"RM"),
            array("key" => "Rf","value"=>"Rf"),
            array("key" => "MTL","value"=>"MTL"),
            array("key" => "UM","value"=>"UM"),
            array("key" => "₮","value"=>"₮"),
            array("key" => "MDL","value"=>"MDL"),
            array("key" => "MT","value"=>"MT"),
            array("key" => "ƒ","value"=>"ƒ"),
            array("key" => "Vt","value"=>"Vt"),
            array("key" => "C$","value"=>"C$"),
            array("key" => "₦","value"=>"₦"),
            array("key" => "B/.","value"=>"B/."),
            array("key" => "PGK","value"=>"PGK"),
            array("key" => "Gs","value"=>"Gs"),
            array("key" => "S/.","value"=>"S/."),
            array("key" => "Php","value"=>"Php"),
            array("key" => "zł","value"=>"zł"),
            array("key" => "lei","value"=>"lei"),
            array("key" => "руб","value"=>"руб"),
            array("key" => "RWF","value"=>"RWF"),
            array("key" => "Db","value"=>"Db"),
            array("key" => "₨","value"=>"₨"),
            array("key" => "Le","value"=>"Le"),
            array("key" => "Sk","value"=>"Sk"),
            array("key" => "₫","value"=>"₫"),
            array("key" => "S","value"=>"S"),
            array("key" => "R","value"=>"R"),
            array("key" => "Z$","value"=>"Z$"),
            array("key" => "MAD","value"=>"MAD"),
            array("key" => "SZL","value"=>"SZL"),
            array("key" => "kr","value"=>"kr"),
            array("key" => "CHF","value"=>"CHF"),
            array("key" => "TJS","value"=>"TJS"),
            array("key" => "฿","value"=>"฿"),
            array("key" => "T$","value"=>"T$"),
            array("key" => "TT$","value"=>"TT$"),
            array("key" => "AED","value"=>"AED"),
            array("key" => "TND","value"=>"TND"),
            array("key" => "₺","value"=>"₺"),
            array("key" => "m","value"=>"m"),
            array("key" => "UGX","value"=>"UGX"),
            array("key" => "₴","value"=>"₴"),
            array("key" => "ден","value"=>"ден"),
            array("key" => "£","value"=>"£"),
            array("key" => "TZS","value"=>"TZS"),
            array("key" => "$","value"=>"$"),
            array("key" => "XOF","value"=>"XOF"),
            array("key" => $U,"value"=>$U),
            array("key" => "лв","value"=>"лв"),
            array("key" => "Bs","value"=>"Bs"),
            array("key" => "XPF","value"=>"XPF"),
            array("key" => "WS$","value"=>"WS$"),
            array("key" => "﷼","value"=>"﷼"),
            array("key" => "ZK","value"=>"ZK")
        );
    }
    /**
     *
     */
    public static function countriesList()
    {
        return array(
            array("key" => "Åland Islands","value"=>"Åland Islands"),
            array("key" => "Afghanistan","value"=>"Afghanistan"),
            array("key" => "Albania","value"=>"Albania"),
            array("key" => "Algeria","value"=>"Algeria"),
            array("key" => "American Samoa","value"=>"American Samoa"),
            array("key" => "Andorra","value"=>"Andorra"),
            array("key" => "Angola","value"=>"Angola"),
            array("key" => "Anguilla","value"=>"Anguilla"),
            array("key" => "Antigua and Barbuda","value"=>"Antigua and Barbuda"),
            array("key" => "Azerbaijan","value"=>"Azerbaijan"),
            array("key" => "Argentina","value"=>"Argentina"),
            array("key" => "Armenia","value"=>"Armenia"),
            array("key" => "Aruba","value"=>"Aruba"),
            array("key" => "Australia","value"=>"Australia"),
            array("key" => "Austria","value"=>"Austria"),
            array("key" => "Bahamas","value"=>"Bahamas"),
            array("key" => "Bahrain","value"=>"Bahrain"),
            array("key" => "Bangladesh","value"=>"Bangladesh"),
            array("key" => "Barbados","value"=>"Barbados"),
            array("key" => "Belgium","value"=>"Belgium"),
            array("key" => "Belarus","value"=>"Belarus"),
            array("key" => "Benin","value"=>"Benin"),
            array("key" => "Bermuda","value"=>"Bermuda"),
            array("key" => "Bhutan","value"=>"Bhutan"),
            array("key" => "Bolivia, Plurinational State of","value"=>"Bolivia, Plurinational State of"),
            array("key" => "Bonaire, Sint Eustatius and Saba","value"=>"Bonaire, Sint Eustatius and Saba"),
            array("key" => "Bosnia and Herzegovina","value"=>"Bosnia and Herzegovina"),
            array("key" => "Botswana","value"=>"Botswana"),
            array("key" => "Bouvet Island","value"=>"Bouvet Island"),
            array("key" => "Brazil","value"=>"Brazil"),
            array("key" => "Belize","value"=>"Belize"),
            array("key" => "British Indian Ocean Territory","value"=>"British Indian Ocean Territory"),
            array("key" => "British Virgin Islands","value"=>" British Virgin Islands"),
            array("key" => "Brunei Darussalam","value"=>"Brunei Darussalam"),
            array("key" => "Bulgaria","value"=>"Bulgaria"),
            array("key" => "Burkina Faso","value"=>"Burkina Faso"),
            array("key" => "Burundi","value"=>"Burundi"),
            array("key" => "Cambodia","value"=>"Cambodia"),
            array("key" => "Cameroon","value"=>"Cameroon"),
            array("key" => "Canada","value"=>"Canada"),
            array("key" => "Cape Verde","value"=>"Cape Verde"),
            array("key" => "Cayman Islands","value"=>"Cayman Islands"),
            array("key" => "Central African Republic","value"=>"Central African Republic"),
            array("key" => "Chad","value"=>"Chad"),
            array("key" => "Chile","value"=>"Chile"),
            array("key" => "China","value"=>"China"),
            array("key" => "Christmas Island","value"=>"Christmas Island"),
            array("key" => "Cocos (Keeling) Islands","value"=>"Cocos (Keeling) Islands"),
            array("key" => "Colombia","value"=>"Colombia"),
            array("key" => "Comoros","value"=>"Comoros"),
            array("key" => "Congo","value"=>"Congo"),
            array("key" => "Congo, the Democratic Republic of the","value"=>"Congo, the Democratic Republic of the"),
            array("key" => "Cook Islands","value"=>"Cook Islands"),
            array("key" => "Costa Rica","value"=>"Costa Rica"),
            array("key" => "Côte d'Ivoire","value"=>"Côte d'Ivoire"),
            array("key" => "Croatia","value"=>"Croatia"),
            array("key" => "Cuba","value"=>"Cuba"),
            array("key" => "Curaçao","value"=>"Curaçao"),
            array("key" => "Cyprus","value"=>"Cyprus"),
            array("key" => "Czech Republic","value"=>"Czech Republic"),
            array("key" => "Denmark","value"=>"Denmark"),
            array("key" => "Djibouti","value"=>"Djibouti"),
            array("key" => "Dominica","value"=>"Dominica"),
            array("key" => "Dominican Republic","value"=>"Dominican Republic"),
            array("key" => "Ecuador","value"=>"Ecuador"),
            array("key" => "El Salvador","value"=>"El Salvador"),
            array("key" => "Egypt","value"=>"Egypt"),
            array("key" => "Equatorial Guinea","value"=>"Equatorial Guinea"),
            array("key" => "Ethiopia","value"=>"Ethiopia"),
            array("key" => "Eritrea","value"=>"Eritrea"),
            array("key" => "Estonia","value"=>"Estonia"),
            array("key" => "Faroe Islands","value"=>"Faroe Islands"),
            array("key" => "Falkland Islands (Malvinas)","value"=>"Falkland Islands (Malvinas)"),
            array("key" => "Fiji","value"=>"Fiji"),
            array("key" => "Finland","value"=>"Finland"),
            array("key" => "France","value"=>"France"),
            array("key" => "French Guiana","value"=>"French Guiana"),
            array("key" => "French Polynesia","value"=>"French Polynesia"),
            array("key" => "French Southern Territories","value"=>"French Southern Territories"),
            array("key" => "Gabon","value"=>"Gabon"),
            array("key" => "Georgia","value"=>"Georgia"),
            array("key" => "Gambia","value"=>"Gambia"),
            array("key" => "Germany","value"=>"Germany"),
            array("key" => "Ghana","value"=>"Ghana"),
            array("key" => "Gibraltar","value"=>"Gibraltar"),
            array("key" => "Greece","value"=>"Greece"),
            array("key" => "Greenland","value"=>"Greenland"),
            array("key" => "Grenada","value"=>"Grenada"),
            array("key" => "Guadeloupe","value"=>"Guadeloupe"),
            array("key" => "Guam","value"=>"Guam"),
            array("key" => "Guatemala","value"=>"Guatemala"),
            array("key" => "Guernsey","value"=>"Guernsey"),
            array("key" => "Guinea","value"=>"Guinea"),
            array("key" => "Guinea-Bissau","value"=>"Guinea-Bissau"),
            array("key" => "Guyana","value"=>"Guyana"),
            array("key" => "Haiti","value"=>"Haiti"),
            array("key" => "Heard Island and McDonald Islands","value"=>"Heard Island and McDonald Islands"),
            array("key" => "Holy See (Vatican City State)","value"=>"Holy See (Vatican City State)"),
            array("key" => "Honduras","value"=>"Honduras"),
            array("key" => "Hong Kong","value"=>"Hong Kong"),
            array("key" => "Hungary","value"=>"Hungary"),
            array("key" => "Iceland","value"=>"Iceland"),
            array("key" => "India","value"=>"India"),
            array("key" => "Indonesia","value"=>"Indonesia"),
            array("key" => "Iran, Islamic Republic of","value"=>"Iran, Islamic Republic of"),
            array("key" => "Iraq","value"=>"Iraq"),
            array("key" => "Ireland","value"=>"Ireland"),
            array("key" => "Isle of Man","value"=>"Isle of Man"),
            array("key" => "Israel","value"=>"Israel"),
            array("key" => "Italy","value"=>"Italy"),
            array("key" => "Jamaica","value"=>"Jamaica"),
            array("key" => "Japan","value"=>"Japan"),
            array("key" => "Jersey","value"=>"Jersey"),
            array("key" => "Jordan","value"=>"Jordan"),
            array("key" => "Kazakhstan","value"=>"Kazakhstan"),
            array("key" => "Kiribati","value"=>"Kiribati"),
            array("key" => "Kenya","value"=>"Kenya"),
            array("key" => "Korea, Democratic People's Republic of","value"=>"Korea, Democratic People's Republic of"),
            array("key" => "Korea, Republic of","value"=>"Korea, Republic of"),
            array("key" => "Kuwait","value"=>"Kuwait"),
            array("key" => "Kyrgyzstan","value"=>"Kyrgyzstan"),
            array("key" => "Lao People's Democratic Republic","value"=>"Lao People's Democratic Republic"),
            array("key" => "Lebanon","value"=>"Lebanon"),
            array("key" => "Lesotho","value"=>"Lesotho"),
            array("key" => "Latvia","value"=>"Latvia"),
            array("key" => "Liberia","value"=>"Liberia"),
            array("key" => "Libya","value"=>"Libya"),
            array("key" => "Liechtenstein","value"=>"Liechtenstein"),
            array("key" => "Lithuania","value"=>"Lithuania"),
            array("key" => "Luxembourg","value"=>"Luxembourg"),
            array("key" => "Macedonia, the former Yugoslav Republic of","value"=>"Macedonia, the former Yugoslav Republic of"),
            array("key" => "Macao","value"=>"Macao"),
            array("key" => "Madagascar","value"=>"Madagascar"),
            array("key" => "Malawi","value"=>"Malawi"),
            array("key" => "Malaysia","value"=>"Malaysia"),
            array("key" => "Maldives","value"=>"Maldives"),
            array("key" => "Mali","value"=>"Mali"),
            array("key" => "Malta","value"=>"Malta"),
            array("key" => "Marshall Islands","value"=>"Marshall Islands"),
            array("key" => "Martinique","value"=>"Martinique"),
            array("key" => "Mauritania","value"=>"Mauritania"),
            array("key" => "Mauritius","value"=>"Mauritius"),
            array("key" => "Mayotte","value"=>"Mayotte"),
            array("key" => "Mexico","value"=>"Mexico"),
            array("key" => "Micronesia, Federated States of","value"=>"Micronesia, Federated States of"),
            array("key" => "Monaco","value"=>"Monaco"),
            array("key" => "Mongolia","value"=>"Mongolia"),
            array("key" => "Moldova, Republic of","value"=>"Moldova, Republic of"),
            array("key" => "Montenegro","value"=>"Montenegro"),
            array("key" => "Montserrat","value"=>"Montserrat"),
            array("key" => "Morocco","value"=>"Morocco"),
            array("key" => "Mozambique","value"=>"Mozambique"),
            array("key" => "Myanmar","value"=>"Myanmar"),
            array("key" => "Namibia","value"=>"Namibia"),
            array("key" => "Nauru","value"=>"Nauru"),
            array("key" => "Nepal","value"=>"Nepal"),
            array("key" => "Netherlands","value"=>"Netherlands"),
            array("key" => "New Caledonia","value"=>"New Caledonia"),
            array("key" => "New Zealand","value"=>"New Zealand"),
            array("key" => "Nicaragua","value"=>"Nicaragua"),
            array("key" => "Niger","value"=>"Niger"),
            array("key" => "Nigeria","value"=>"Nigeria"),
            array("key" => "Niue","value"=>"Niue"),
            array("key" => "Norfolk Island","value"=>"Norfolk Island"),
            array("key" => "Norway","value"=>"Norway"),
            array("key" => "Northern Mariana Islands","value"=>"Northern Mariana Islands"),
            array("key" => "Oman","value"=>"Oman"),
            array("key" => "Palau","value"=>"Palau"),
            array("key" => "Pakistan","value"=>"Pakistan"),
            array("key" => "Palestinian Territory, Occupied","value"=>"Palestinian Territory, Occupied"),
            array("key" => "Panama","value"=>"Panama"),
            array("key" => "Papua New Guinea","value"=>"Papua New Guinea"),
            array("key" => "Paraguay","value"=>"Paraguay"),
            array("key" => "Peru","value"=>"Peru"),
            array("key" => "Philippines","value"=>"Philippines"),
            array("key" => "Pitcairn","value"=>"Pitcairn"),
            array("key" => "Poland","value"=>"Poland"),
            array("key" => "Portugal","value"=>"Portugal"),
            array("key" => "Puerto Rico","value"=>"Puerto Rico"),
            array("key" => "Qatar","value"=>"Qatar"),
            array("key" => "Réunion","value"=>"Réunion"),
            array("key" => "Romania","value"=>"Romania"),
            array("key" => "Russian Federation","value"=>"Russian Federation"),
            array("key" => "Rwanda","value"=>"Rwanda"),
            array("key" => "Saint Barthélemy","value"=>"Saint Barthélemy"),
            array("key" => "Saint Helena, Ascension and Tristan da Cunha","value"=>"Saint Helena, Ascension and Tristan da Cunha"),
            array("key" => "Saint Kitts and Nevis","value"=>"Saint Kitts and Nevis"),
            array("key" => "Saint Lucia","value"=>"Saint Lucia"),
            array("key" => "Saint Martin (French part)","value"=>"Saint Martin (French part)"),
            array("key" => "Saint Pierre and Miquelon","value"=>"Saint Pierre and Miquelon"),
            array("key" => "Saint Vincent and the Grenadines","value"=>"Saint Vincent and the Grenadines"),
            array("key" => "Samoa","value"=>"Samoa"),
            array("key" => "San Marino","value"=>"San Marino"),
            array("key" => "Sao Tome and Principe","value"=>"Sao Tome and Principe"),
            array("key" => "Saudi Arabia","value"=>"Saudi Arabia"),
            array("key" => "Senegal","value"=>"Senegal"),
            array("key" => "Serbia","value"=>"Serbia"),
            array("key" => "Seychelles","value"=>"Seychelles"),
            array("key" => "Sierra Leone","value"=>"Sierra Leone"),
            array("key" => "Singapore","value"=>"Singapore"),
            array("key" => "Sint Maarten (Dutch part)","value"=>"Sint Maarten (Dutch part)"),
            array("key" => "Slovakia","value"=>"Slovakia"),
            array("key" => "Slovenia","value"=>"Slovenia"),
            array("key" => "Solomon Islands","value"=>"Solomon Islands"),
            array("key" => "Somalia","value"=>"Somalia"),
            array("key" => "South Africa","value"=>"South Africa"),
            array("key" => "South Georgia and the South Sandwich Islands","value"=>"South Georgia and the South Sandwich Islands"),
            array("key" => "Sri Lanka","value"=>"Sri Lanka"),
            array("key" => "Spain","value"=>"Spain"),
            array("key" => "South Sudan","value"=>"South Sudan"),
            array("key" => "Sudan","value"=>"Sudan"),
            array("key" => "Suriname","value"=>"Suriname"),
            array("key" => "Svalbard and Jan Mayen","value"=>"Svalbard and Jan Mayen"),
            array("key" => "Swaziland","value"=>"Swaziland"),
            array("key" => "Sweden","value"=>"Sweden"),
            array("key" => "Switzerland","value"=>"Switzerland"),
            array("key" => "Syrian Arab Republic","value"=>"Syrian Arab Republic"),
            array("key" => "Tajikistan","value"=>"Tajikistan"),
            array("key" => "Thailand","value"=>"Thailand"),
            array("key" => "Togo","value"=>"Togo"),
            array("key" => "Tokelau","value"=>"Tokelau"),
            array("key" => "Tonga","value"=>"Tonga"),
            array("key" => "Trinidad and Tobago","value"=>"Trinidad and Tobago"),
            array("key" => "Taiwan, Province of China","value"=>"Taiwan, Province of China"),
            array("key" => "Tanzania, United Republic of","value"=>"Tanzania, United Republic of"),
            array("key" => "Timor-Leste","value"=>"Timor-Leste"),
            array("key" => "Tunisia","value"=>"Tunisia"),
            array("key" => "Turkey","value"=>"Turkey"),
            array("key" => "Turkmenistan","value"=>"Turkmenistan"),
            array("key" => "Turks and Caicos Islands","value"=>"Turks and Caicos Islands"),
            array("key" => "Tuvalu","value"=>"Tuvalu"),
            array("key" => "Uganda","value"=>"Uganda"),
            array("key" => "Ukraine","value"=>"Ukraine"),
            array("key" => "United Kingdom","value"=>"United Kingdom"),
            array("key" => "United Arab Emirates","value"=>"United Arab Emirates"),
            array("key" => "United States","value"=>"United States"),
            array("key" => "United States Minor Outlying Islands","value"=>"United States Minor Outlying Islands"),
            array("key" => "Uruguay","value"=>"Uruguay"),
            array("key" => "Uzbekistan","value"=>"Uzbekistan"),
            array("key" => "Vanuatu","value"=>"Vanuatu"),
            array("key" => "Venezuela, Bolivarian Republic of","value"=>"Venezuela, Bolivarian Republic of"),
            array("key" => "Vietnam","value"=>"Vietnam"),
            array("key" => "Virgin Islands, U.S.","value"=>"Virgin Islands, U.S."),
            array("key" => "Wallis and Futuna","value"=>"Wallis and Futuna"),
            array("key" => "Western Sahara","value"=>"Western Sahara"),
            array("key" => "Yemen","value"=>"Yemen"),
            array("key" => "Zambia","value"=>"Zambia"),
            array("key" => "Zimbabwe","value"=>"Zimbabwe"),
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function countriesEnglishArabic()
    {
        $data = json_decode(file_get_contents('countries_english_arabic.json'), true);
        return HelperModule::jsonApiResponse(true, Lang::get('messages.success.countries_list'),$data);
    }
    /**
     * @return array
     * Months List in Array
     */
    public static function monthsList()
    {
        return array(
            array(
                "key" => 0,
                "value" =>  Lang::get('frontend.month.select_month')
                ),
            array(
                "key" => 1,
                "value" => Lang::get('frontend.month.january')
            ),
            array(
                "key" => 2,
                "value" => Lang::get('frontend.month.february')
            ),
            array(
                "key" => 3,
                "value" => Lang::get('frontend.month.march')
            ),
            array(
                "key" => 4,
                "value" => Lang::get('frontend.month.april')
            ),
            array(
                "key" => 5,
                "value" => Lang::get('frontend.month.may')
            ),
            array(
                "key" => 6,
                "value" => Lang::get('frontend.month.june')
            ),
            array(
                "key" => 7,
                "value" => Lang::get('frontend.month.july')
            ),
            array(
                "key" => 8,
                "value" => Lang::get('frontend.month.august')
            ),
            array(
                "key" => 9,
                "value" => Lang::get('frontend.month.september')
            ),
            array(
                "key" => 10,
                "value" => Lang::get('frontend.month.october')
            ),
            array(
                "key" => 11,
                "value" => Lang::get('frontend.month.november')
            ),
            array(
                "key" => 12,
                "value" => Lang::get('frontend.month.december')
            ),
        );
    }

    /**
     * @return array
     */
    public static function yearsList()
    {
        // Year to start available options at
        $earliest_year = 1920;
        // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
        $latest_year = date('Y');
        $list = [["key" => 0,
            "value" => Lang::get('frontend.select_year')]];
        foreach ( range( $latest_year, $earliest_year ) as $i ) {
            $newArray = ["key" => $i,
                "value" => $i
            ];
            array_push($list,$newArray);
        }
        return $list;
        //print_r($list);
    }
}
