<?php

namespace App;

use App\HelperModules\HelperModule;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'category_name', 'category_id','category_level ', 'category_descripition' , 'category_image'
    ];

    public function products(){
        return $this->belongsToMany(Product::class, 'productcategory', 'category_id', 'product_id');
    }
    public function getParentCategory($id){
        $name = category::find($id);
        if($name)
          return $name->category_name;

        return 'N/A';
    }
    public function getParentCategoryId($id){
        $name = category::find($id);
        if($name)
          return $name->id;

        return 'null';
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class)->with('categories');
    }

    public static function getParentCategories(){
        $parent_categoreis = Self::whereNull('category_id')->get();
       return $parent_categoreis;
    }

    public static function getChildCategories(){
        $child_categoreis = Self::whereNotNull('category_id')->get();
       return $child_categoreis;
    }

    public static function getSpecificCategoryChildCategories($category_id){
        $child_categoreis = Self::where('category_id',$category_id)->get();
        return $child_categoreis;
    }
}
