<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name','slug'];

    public function assignRoom(){
        return $this->hasMany(AssignedRoom::class,'room_id');
    }
    public function checkRoomAssignedToLocation($location,$room_id){
        $result = AssignedRoom::where('location_id',$location)->where('room_id',$room_id)->first();
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function roomMeta(){
        return $this->belongsTo(RoomMetaData::class,'room_id');
    }
}
