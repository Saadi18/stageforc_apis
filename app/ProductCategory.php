<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'products_category';
    public function parentCategory(){
        return $this->belongsTo(Category::class,'parent_category_id','id');
    }
    public function childCategory(){
        return $this->belongsTo(Category::class,'child_category_id','id');
    }
//    public function product(){
//        return $this->belongsTo(Category::class,'category_id','id');
//    }

    public static function getParentAssignedCategories(){
        $parent_categoreis = Self::whereNull('child_category_id')->get();
        return $parent_categoreis;
    }
    public static function getAssignedChildCategories(){
        $parent_categoreis = Self::whereNotNull('child_category_id')->get();
        return $parent_categoreis;
    }
}
