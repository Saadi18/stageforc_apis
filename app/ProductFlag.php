<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProductFlag extends Model
{
    protected $table = 'product_flags';
    protected $fillable = ['issue_type','rfid','product_id'];

}
