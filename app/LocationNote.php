<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationNote extends Model
{
    protected $table = 'location_notes';
    protected $fillable = ['location_id','note','note_by'];
}
