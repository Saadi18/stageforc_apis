<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'location_name', 'location_description','product','street_address','city', 'state', 'zip_code',
        'home_status', 'access_code', 'data_of_stage', 'date_of_destage' , 'location_square_feet' , 'price_estimate' , 'image'
    ];


    protected $table = "location";
}
