<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSelection extends Model
{
    protected $table = 'products_selection';
    protected $fillable = ['product_id','location_id','product_rfid','status'];

}
