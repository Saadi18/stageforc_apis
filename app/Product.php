<?php

namespace App;

use App\Models\ProductQuantity;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_name', 'product_description','sku','working_value', 'vendor', 'length',
        'width', 'height','location_id', 'purchase_date', 'purchase_price' , 'rental_price' , 'sale_price' , 'image'
    ];
    public function categories(){
        return $this->belongsToMany('App\Category', 'products_category', 'product_id', 'category_id');
    }

    public function assignedCategories(){
        return $this->hasMany(ProductCategory::class,'product_id','id');
    }

    public function getAssignedParentCategories(){
        return $this->hasMany(ProductCategory::class,'product_id','id');
    }
    public function getAssignedChildCategories(){
        return $this->hasMany(ProductCategory::class,'product_id','id')
            ->whereNotNull('child_category_id');
    }

    public function quantity(){
        return $this->hasMany(ProductQuantity::class,'product_id','id');
    }
}
