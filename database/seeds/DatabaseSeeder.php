<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'John',
            'last_name' => 'John',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $role = \Spatie\Permission\Models\Role::create(['name' => 'Admin']);

        $permissions = \App\Models\Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
