<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['prefix' => 'auth', 'namespace' => 'Api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('permission_list', 'AuthController@getUserPermissions');

        Route::group(['prefix' => 'products'],function(){
            Route::post('search','ProductController@searchProducts');
            Route::post('add-quantity','ProductController@addMoreQuantity');
            Route::get('get-quantity','ProductController@getProductQuantity');
            Route::post('assign-category','ProductController@assignCategory');
            Route::get('get-assigned-category','ProductController@getAssignedCategories');
            Route::post('assign-rfids','ProductController@assignRfids');
            Route::get('all-rfids','ProductController@getAllRfids');
            Route::post('assign-product-flag','ProductController@assignProductFlag');
            Route::get('all-flags','ProductController@getAllFlags');
            Route::get('sortProduct','ProductController@sortProduct');
            Route::post('delete-rfid','ProductController@deleteRfid');
            Route::post('delete-flag','ProductController@deleteFlag');
            Route::post('update','ProductController@update');

        });
        Route::get('get-not-assigned-rfids','ProductController@getNotAssginedRfids');
        Route::apiResource('products','ProductController');
        Route::apiResource('roles','RoleController');
        Route::get('show_role_detail','RoleController@showRoleDetai');
        Route::apiResource('permissions','PermissionController');
        Route::apiResource('modules','ModuleController');
        Route::apiResource('vendors','VendorController');
        Route::group(['prefix' => 'vendors'],function(){
            Route::post('update','VendorController@update');
        });
        Route::get('get-vendors','VendorController@getVendorsList');
        Route::get('parent-child-cats','CategoryController@parentChildCats');
        Route::get('delete-assigned-cat','CategoryController@deleteAssignedCat');

        Route::get('get-parent-cats','CategoryController@getParentCats');
        Route::get('get-cats-data','CategoryController@getCatsData');

        Route::apiResource('categories','CategoryController');
        Route::group(['prefix' => 'categories'],function(){
            Route::post('update','CategoryController@update');
        });
        Route::apiResource('locations','LocationController');
        Route::post('locations/update','LocationController@update');
        Route::post('update/user','UserController@update');
        Route::apiResource('users','UserController');
        Route::post('product-selection','ProductSelectionController@productSelection');
        Route::post('remove-selection','ProductSelectionController@removeSelection');
        Route::post('pause-selection','ProductSelectionController@pauseSelection');
        Route::post('resume-selection','ProductSelectionController@resumeSelection');
        Route::post('finish-selection','ProductSelectionController@finishSelection');
        Route::get('get-selected-products','ProductSelectionController@getAllSelectedProducts');
        Route::post('mark-located','ProductSelectionController@markeLocated');

        Route::get('all-rooms','RoomController@index');
        Route::post('room-detail','RoomController@roomDetail');
        Route::post('update-room','RoomController@updateRoom');
        Route::post('create','RoomController@create');
        Route::post('assign-room','RoomController@assign_room');
        Route::post('assign-room-lists','RoomController@assignedRoomList');
        Route::post('delete-assign-room','RoomController@deleteAssignedRoom');
        Route::get('assigned-notes','NotesController@index');
        Route::post('create-note','NotesController@create');
        Route::get('edit-note','NotesController@edit');
        Route::post('update-note','NotesController@update');
        Route::get('delete-note','NotesController@delete');

    });
});
